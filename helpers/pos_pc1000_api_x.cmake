include(${CMAKE_CURRENT_LIST_DIR}/POS_PC1000_api_dirs.cmake)

include_directories(${POS_PC1000_API_INCLUDE})
link_directories(${POS_PC1000_API_LIB})

list(APPEND CMAKE_FIND_ROOT_PATH "${POS_PC1000_API_DIR}")
