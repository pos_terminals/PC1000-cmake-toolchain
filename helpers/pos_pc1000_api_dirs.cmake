if(DEFINED POS_PC1000_API_DIR)
    return()
endif()

include(${CMAKE_CURRENT_LIST_DIR}/pos_pc1000_sdk_dir.cmake)

if(NOT DEFINED POS_PC1000_API_VER)
    message(FATAL_ERROR "We must set API Version (POS_PC1000_API_VER=<2|3>")
    return()
endif()

set(POS_PC1000_API_DIR "${POS_PC1000_SDK_DIR}/API_${POS_PC1000_API_VER}")
set(POS_PC1000_API_INCLUDE "${POS_PC1000_API_DIR}/include")
set(POS_PC1000_API_LIB "${POS_PC1000_API_DIR}/lib")
